import sys
import pyshark
import pandas as pd

if len(sys.argv) > 1:
    capture_name = sys.argv[1]
else:
    capture_name = 'training_data_pdus_to_file.pcapng'

cap = pyshark.FileCapture(capture_name)

packets = []
for p in cap:
    packet = {}
    for i in p.layers:
        for j in i.field_names:
            packet[i.layer_name + '|' + j] = i.get(j)
    packets.append(packet)

df = pd.DataFrame(packets)
print(len(df.columns))
print(df.columns)
df.to_csv(capture_name + '.csv')
